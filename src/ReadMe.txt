// Easton Anderson, Minh Huynh, Petr Shuller
// CS240 with Ryan Parsons
// Sorting Points lab

1.
insertionSort: O(n^2) because we have one loop inside of another.
selectionSort: O(n^2) because findMin() costs O(n) and the outer loop costs another O(n)
heapSort: O(nlogn) because heapify costs O(nlogn), for loop only costs O(n)
mergeSort: O(nlogn) because merge does one for loop for every logn branch that needs to be merged

2. 
5, 7, 9, 1, 3, 4, 6, 8, 2
#           #           #
3, 7, 9, 1, 5, 4, 6, 8, 2
*  !                    !
3, 2, 9, 1, 5, 4, 6, 8, 7
*     !              !
3, 2, 9, 1, 5, 4, 6, 8, 7
*     !           !
3, 2, 8, 1, 5, 4, 6, 9, 7
*     !        !
3, 2, 8, 1, 5, 4, 6, 9, 7
*     !     !
3, 2, 8, 1, 5, 4, 6, 9, 7
*     !  !
3, 2, 1, 8, 5, 4, 6, 9, 7
*    !!
1, 2, 3, 8, 5, 4, 6, 9, 7
      *
1, 2, 3, 8, 5, 4, 6, 9, 7 (selection sort for 1, 2)
         #     #        #
1, 2, 3, 7, 5, 4, 6, 9, 8
         *  !           !
1, 2, 3, 7, 5, 4, 6, 9, 8
         *     !     !
1, 2, 3, 7, 5, 4, 6, 9, 8
         *       !!
1, 2, 3, 6, 5, 4, 7, 9, 8
                  *
1, 2, 3, 4, 5, 6, 7, 8, 9 (selection sort for 6, 5, 4 and 9, 8)
                  *
   

3.
  abc     da      b      b      b      b      b  
   da     ca    abc     ca     ca     ca     ca  
 ffff      b  abebd     da     da     da     da  
defcd    abc     ca    abc    abc    abc    abc  
abebd  defcd  defcd    dfe    dfe    dfe    dfe  
   ca  abebd     da  abebd    fef    fef    fef  
    b    dfe    fef  defcd  abebd   ffff   ffff  
  fef   ffff    dfe    fef  defcd  abebd  abebd  
  dfe    fef   ffff   ffff   ffff  defcd  defcd  
