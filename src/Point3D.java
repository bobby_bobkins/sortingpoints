// Easton Anderson, Minh Huynh, Petr Shuller
// CS240 with Ryan Parsons
// Sorting Points lab

public class Point3D implements Comparable<Point3D> {
    private int x, y, z;

    // Creates a default 3D points with all coordinates set to 0
    public Point3D() {
        x = y = z = 0;
    }

    // Creates a 3D point with the given coordinates
    public Point3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // Post: Returns the x coordinate for the 3D point
    public int getX() {
        return x;
    }

    // Post: Returns the y coordinate for the 3D point
    public int getY() {
        return y;
    }

    // Post: Returns the z coordinate for the 3D point
    public int getZ() {
        return z;
    }

    // Pre: A Point3D to compare ourselves to
    // Post: Compares based on z values, then x, then y.
    //      Returns -1 if we are smaller than other, 0 if equal, 1 if we are larger
    public int compareTo(Point3D other) {
        int comparison;

        if (other.getZ() == z) {
            if (other.getX() == x) {
                if (other.getY() == y) {
                    comparison = 0;
                } else {
                    comparison = other.getY() > y ? -1 : 1;
                }
            } else {
                comparison = other.getX() > x ? -1 : 1;
            }
        } else {
            comparison = other.getZ() > z ? -1 : 1;
        }

        return comparison;
    }

    // Post: Returns string representation of our point
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }
}
