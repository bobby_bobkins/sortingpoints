import java.awt.Color;
import java.awt.Graphics;
import java.util.*;

// Easton Anderson, Minh Huynh, Petr Shuller
// CS240 with Ryan Parsons
// Sorting Points lab

// Ryan Parsons
// May 23rd, 2017
// Version 2.0

/* 
   Simple program to display 3 dimensional point objects on a graphical interface.
   Points use the z coordinate to determine which is on top. Several different sorting
   algorithms are used (to be implemented by you) to sort the elements into the proper
   ordering for display.
*/

public class SortingMain {

    // Set to false if you want output that varies each time
    public static final boolean DEBUG = false;

    // Radius for the circles drawn
    public static final int RADIUS = 20;

    // Maximum z value for Point3D objects
    public static final int Z_MAX = 50;

    // Number of points to get displayed
    public static final int NUM_POINTS = 10000;

    public static void main(String[] args) {
        // Setting up the graphics
        DrawingPanel p = new DrawingPanel(600, 600);
        p.setBackground(Color.CYAN);
        Graphics g = p.getGraphics();
        int panelWidth = p.getWidth();
        int panelHeight = p.getHeight();

        Random r;
        if (DEBUG) {
            // If debugging/testing uses a seed of 42 to ensure the same reasults each time
            r = new Random(42);
        } else {
            r = new Random();
        }

        List<Point3D> points = makeList(r, panelWidth, panelHeight);

        drawPoints(points, g, r);
    }

    // Takes in a Random object and creates and returns a new list of NUM_ POINTS Point3D objects
    // with random x, y, and z coordinates
    // x and y coordinates are bounded by the current window size of the
    // DrawingPanel, while z is bounded by a class constant
    public static List<Point3D> makeList(Random r, int panelWidth, int panelHeight) {
        List<Point3D> list = new ArrayList<Point3D>();
        for (int i = 0; i < NUM_POINTS; i++) {
            int x = r.nextInt(panelWidth - RADIUS);
            int y = r.nextInt(panelHeight - RADIUS);
            int z = r.nextInt(Z_MAX);
            list.add(new Point3D(x, y, z));
        }
        return list;
    }

    // Given a list of Point3D objects, a Graphics object, and a Random objects, draws the points
    // in the provided list on the DrawingPanel
    public static void drawPoints(List<Point3D> points, Graphics g, Random r) {
        // Array of colors to randomly choose a color from when drawing a point
        Color[] colors = {Color.RED, Color.BLUE, Color.YELLOW, Color.GREEN, Color.MAGENTA,
                Color.GRAY, Color.PINK, Color.WHITE, Color.BLACK};

        // Array to be sorted
        Point3D[] pointArray = (Point3D[]) points.toArray(new Point3D[0]);

       /*
      
         call various sort methods here
         all sort methods should result in the same output
         
         insertionSort
         selectionSort
         heapSort (in-place)
         mergeSort
      
      */

        for (int i = pointArray.length - 1; i >= 0; i--) {
            g.setColor(colors[r.nextInt(colors.length)]);
            g.fillOval(pointArray[i].getX(), pointArray[i].getY(), RADIUS, RADIUS);
        }
    }

    // Pre: Point3D array to be sorted
    // Post: Modifies provided array to be sorted via insertion sort
    public static void insertionSort(Point3D[] points) {
        for (int i = 1; i < points.length; i++) {

            if (points[i].compareTo(points[0]) == -1) {
                insertShift(points, 0, i);
            } else {
                for (int j = i - 1; j > 0; j--) {
                    if (points[i].compareTo(points[j]) == 0 ||
                            (points[i].compareTo(points[j]) == -1 && points[i].compareTo(points[j - 1]) == 1)) {
                        insertShift(points, j, i);
                    }
                }
            }
        }
    }

    // Pre: Takes a Point3D array as a parameter
    // Post: Sort provided array with selection sort
    public static void selectionSort(Point3D[] points) {
        //Finds minimum number in array and swaps it with current spot
        for (int x = 0; x < points.length; x++) {
            int min = findMin(points, x);
            swap(points, x, min);
        }
    }

    // Pre: Point3D array to be sorted
    // Post: Modifies provided array to be sorted via an in-place heapsort
    public static void heapSort(Point3D[] points) {
        heapify(points);

        // Delete, swap, fix heap
        for (int i = 0; i < points.length; i++) {
            int heapEnd = points.length - 1 - i;

            swap(points, 0, heapEnd);
            percolateDown(points, 0, heapEnd - 1);
        }
    }

    // Pre: Point3D array to be sorted
    // Post: Modifies provided array to be sorted via mergesort
    public static void mergeSort(Point3D[] points) {
        mergeSort(points, new Point3D[points.length], 0, points.length - 1);
    }

    // Private recursive helper method for mergesort
    // Pre: takes in Point3D array to be sorted, as well as an additional Point3D array as temp storage
    //      Requires start and end indecy range we are to sort in
    // Post: Modifies provided array to be sorted within given range
    private static void mergeSort(Point3D[] points, Point3D[] auxilary, int start, int end) {
        if (end - start < 1) {
            return;
        }

        int mid = (end + start) / 2;

        mergeSort(points, auxilary, start, mid);
        mergeSort(points, auxilary, mid + 1, end);
        merge(points, auxilary, start, end);

    }

    // Pre: Point3D array to be shifted. indexGrab is number you are taking, indexInsert is where you are putting in and shifting from
    // Post: Performs insert and shift on provided array
    private static void insertShift(Point3D[] points, int indexInsert, int indexGrab) {
        Point3D saved = points[indexGrab];

        for (int i = indexGrab; i > indexInsert; i--) {
            points[i] = points[i - 1];
        }

        points[indexInsert] = saved;
    }

    // Pre: Point3D array, starting index to look at
    // Post: Finds the index of the minimum point in the array after provided starting point
    private static int findMin(Point3D[] points, int start) {
        int min = start;
        for (int x = start + 1; x < points.length; x++) {
            if (points[x].compareTo(points[min]) < 0) {
                min = x;
            }
        }
        return min;
    }

    // Pre: Point3D array to be heapified
    // Post: Turns array into max heap representation
    private static void heapify(Point3D[] array) {
        int size = array.length;

        for (int i = (size - 1) / 2; i >= 0; i--) {
            percolateDown(array, i, (size - 1));
        }
        System.out.println("Max heapified:   " + Arrays.toString(array));
    }

    // Pre: Point3D array, index we want to percolate down, and index for end of heap
    // Post: Percolates down value at index through our heap section
    private static void percolateDown(Point3D[] array, int index, int heapEnd) {
        int maxChild = maxChild(array, index, heapEnd);

        if (maxChild <= heapEnd && maxChild > 0 && array[index].compareTo(array[maxChild]) == -1) {
            swap(array, index, maxChild);
            percolateDown(array, maxChild, heapEnd);
        }
    }

    // Pre: Point3D array, two indices to swap. Assumes indices are valid.
    // Post: Swaps the two elements
    private static void swap(Point3D[] array, int indexOne, int indexTwo) {
        //System.out.println("Swapping: " + indexOne +  " and " + indexTwo);
        Point3D temp = array[indexOne];
        array[indexOne] = array[indexTwo];
        array[indexTwo] = temp;
    }

    // Pre: Takes an array, an index whose max child we wish to find, and end index of our heap representation
    // Post: Returns the index of the greatest value child.
    private static int maxChild(Point3D[] array, int index, int heapEnd) {
        int left = index * 2 + 1;
        int right = left + 1;

        int indexMax = left;

        if (left <= heapEnd) {
            if (right <= heapEnd && array[right].compareTo(array[indexMax]) == 1) {
                indexMax = right;
            }
        } else {
            return -1; // We don't have children
        }

        return indexMax;
    }

    // Pre: Point3D arrays, first that holds our values, second to be used as temp storage.
    //      Requires two indices to know range we are merging
    // Post: Performs mergesort merge to end up with sorted values in our index range
    private static void merge(Point3D[] points, Point3D[] auxiliary, int start, int end) {
        int mid = (start + end) / 2;
        int left = start;
        int right = mid + 1;

        // Do the merge
        for (int i = start; i <= end; i++) {
            // Case where both can be incremented
            if (left <= mid && right <= end) {
                if (points[left].compareTo(points[right]) == -1) {
                    auxiliary[i] = points[left++];
                } else {
                    auxiliary[i] = points[right++];
                }
            } else if (left <= mid) {
                auxiliary[i] = points[left++];
            } else {
                auxiliary[i] = points[right++];
            }
        }

        // Copy back to original because it is easier for me to think about it that way
        for (int i = start; i <= end; i++) {
            points[i] = auxiliary[i];
        }
    }
}